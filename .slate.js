// Configs
S.cfga({
  "defaultToCurrentScreen" : true,
  "secondsBetweenRepeat" : 0.1,
  "checkDefaultsOnLoad" : true,
  "focusCheckWidthMax" : 3000,
  "orderScreensLeftToRight" : true
});

// Monitors
var monTbolt  = "1920x1080";
var monLaptop = "1280x800";

// Operations
var fullScreen = S.op("move", {
  "x" : "screenOriginX",
  "y" : "screenOriginY",
  "width" : "screenSizeX",
  "height" : "screenSizeY"
});

var leftHalf = S.op("move", {
  "x" : "screenOriginX",
  "y" : "screenOriginY",
  "width" : "screenSizeX/2",
  "height" : "screenSizeY",
});

var rightHalf = S.op("move", {
  "x" : "screenOriginX+screenSizeX/2",
  "y" : "screenOriginY",
  "width" : "screenSizeX/2",
  "height" : "screenSizeY",
});


var lapFull = S.op("move", {
  "screen" : monLaptop,
  "x" : "screenOriginX",
  "y" : "screenOriginY",
  "width" : "screenSizeX",
  "height" : "screenSizeY"
});

var tboltFull = S.op("move", {
  "screen" : monTbolt,
  "x" : "screenOriginX",
  "y" : "screenOriginY",
  "width" : "screenSizeX",
  "height" : "screenSizeY"
});

var logHello = function(){
    S.log("Hello");
};

var grid = slate.operation("grid", {
  "grids" : {
    "1920x1080" : {
      "width" : 2,
      "height" : 2
    },
    "1280x800" : {
      "width" : 2,
      "height" : 2
    }
  },
  "padding" : 2
});

var throwRight = S.op("throw", {
    "screen" : "right",
    "width" : S.app().mainWindow().rect().width,
    "height" : S.app().mainWindow().rect().height
})

var throwLeft = S.op("throw", {
    "screen" : "left",
    "width" : S.app().mainWindow().rect().width,
    "height" : S.app().mainWindow().rect().height
})

var getScreen = function(){
    S.log(S.screen().id());
}

var tboltLeft = tboltFull.dup({ "width" : "screenSizeX/3" });
var tboltMid = tboltLeft.dup({ "x" : "screenOriginX+screenSizeX/3" });
var tboltRight = tboltLeft.dup({ "x" : "screenOriginX+(screenSizeX*2/3)" });

S.bnda({
    "w:cmd;ctrl": fullScreen,
    "l:cmd;ctrl": leftHalf,
    ";:cmd;ctrl": rightHalf,
    "r:cmd;ctrl": S.op("relaunch"),
    "s:cmd;ctrl": getScreen,
    "e:cmd;ctrl" : S.op("hint"),
    "g:cmd;ctrl" : grid,

    //nudge bindings
    "right:cmd;ctrl" : S.op("nudge", { "x" : "+10%", "y" : "+0" }),
    "left:cmd;ctrl" : S.op("nudge", { "x" : "-10%", "y" : "+0" }),
    "up:cmd;ctrl" : S.op("nudge", { "x" : "+0", "y" : "-10%" }),
    "down:cmd;ctrl" : S.op("nudge", { "x" : "+0", "y" : "+10%" }),

    // focus bindings
    "l:cmd;shift" : S.op("focus", { "direction" : "right" }),
    "h:cmd;shift" : S.op("focus", { "direction" : "left" }),
    "k:cmd;shift" : S.op("focus", { "direction" : "up" }),
    "j:cmd;shift" : S.op("focus", { "direction" : "down" }),
    "b:cmd;shift" : S.op("focus", { "direction" : "behind" }),
    "a:cmd;shift" : S.op("focus", { "direction" : "below" }),

    "v:cmd;shift" : S.op("focus", { "app": "MacVim" }),
    "i:cmd;shift" : S.op("focus", { "app": "iTerm" }),
    "c:cmd;shift" : S.op("focus", { "app": "Google Chrome" }),
    "p:cmd;shift" : S.op("focus", { "app": "Preview" }),
    

    //resize bindings
    "right:alt;shift" : S.op("resize", { "width" : "+10%", "height" : "+0" }),
    "left:alt;shift" : S.op("resize", { "width" : "-10%", "height" : "+0" }),
    "up:alt;shift" : S.op("resize", { "width" : "+0", "height" : "-10%" }),
    "down:alt;shift" : S.op("resize", { "width" : "+0", "height" : "+10%" }),

    // throw bindings
    "right:ctrl;alt;cmd" : throwRight,
    "left:ctrl;alt;cmd" : throwLeft,
})
