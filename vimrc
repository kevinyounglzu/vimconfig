"基本功能
"""""""""""""""""""""""""""""""""""""""""""""""""""""

"不要兼容vi
set nocompatible

"不要自动折叠
set nofoldenable

"允许鼠标的使用
set mouse=a

"光标移动到顶部和底部的时候保留三行的距离
set scrolloff=3

"禁止生成临时文件
set nobackup
set noswapfile

"文件被改变时自动读取
set autoread

"显示相对行号
set relativenumber number
autocmd InsertEnter * :set norelativenumber number
autocmd InsertLeave * :set relativenumber number

"回删设置
set backspace=indent,eol,start

"打开文件时回到上次退出时光标的位置
autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal g`\"" |
    \ endif

"打开wildmenu，用于命令行补完。比如在命令行输入 :color <Tab> 会出现一排电脑上可用的色彩方案，按<Tab>就能切换
set wildmenu

"总显示当前位置，见右下角
set ruler

"统一编码
"set encoding=utf8
set fileencodings=utf-8,gbk

set nrformats=
"""""""""""""""""""""""""""""""""""""""""""""""""""""








"GUI设置
"""""""""""""""""""""""""""""""""""""""""""""""""""""
"取消MacVim的铃声
autocmd! GUIEnter * set vb t_vb=

"""""""""""""""""""""""""""""""""""""""""""""""""""""









"搜索设置
"""""""""""""""""""""""""""""""""""""""""""""""""""""

"高亮搜索结果
set hlsearch

"回车暂时取消搜索高亮
nnoremap <CR> :nohlsearch<cr>

"在输入搜索文字时，实时匹配
set incsearch

"搜索时忽略大小写
set ignorecase

"输入大写字母时会严格的匹配，输入小写时配合上面一条则也可以匹配大写
set smartcase

"显示配对的括号
set showmatch
"""""""""""""""""""""""""""""""""""""""""""""""""""""
















"缩进/tab/行显示
"""""""""""""""""""""""""""""""""""""""""""""""""""""

"将tab换成空格
set expandtab

"智能tab
set smarttab

"自动缩进
set autoindent

"智能缩进
set smartindent
"smartindent for c
"set cindent

"在缩进不成整数时自动补全
set shiftround

"1tab==4空格
set shiftwidth=4
set tabstop=4

"自动断行
set lbr
set tw=500

"自动换行
set wrap
"""""""""""""""""""""""""""""""""""""""""""""""""""""













"快捷键映射
"""""""""""""""""""""""""""""""""""""""""""""""""""""
"经常按错
map q <nop>

"缩进以后保持选定
vmap < <gv
vmap > >gv

"设置一下leader键
let mapleader = '\'

"
map <space> :

"将jj映射到escape
inoremap jj <Esc>

nnoremap Y y$

"在分屏中间方便移动的映射
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l

"将j和k的移动由逻辑行转为屏幕行
noremap j gj
noremap gj j
noremap k gk
noremap gk k

inoremap kk <C-o>

"file navigation
"cnoremap %% <C-R>=expand('%:h').'/'<cr>
"noremap <leader>e :edit %%
"noremap <leader>v :view %%

cnoremap ev split $MYVIMRC<cr>
cnoremap sv so $MYVIMRC<cr>

"cnoremap <C-p> <Up>
"cnoremap <C-n> <Down>

"F5 插入时间
inoremap <F5> <C-R>=strftime("%F")<CR>

"方便移动光标至行首和行尾
noremap H g^
noremap L g$


"
nnoremap U <c-r>
"""""""""""""""""""""""""""""""""""""""""""""""""""""





"针对不同文件类型的设置
"""""""""""""""""""""""""""""""""""""""""""""""""""""
source ~/.vim/var.vim

"开启文档类型侦测
filetype on

"打开自动高亮
syntax on

"python
"autocmd FileType python set columns=90 lines=50
autocmd FileType python set cul cuc colorcolumn=81
autocmd BufNewFile *.py 0r $pythonheader

"autocmd FileType python noremap <leader>r :w\|:!python %<cr>
autocmd FileType python noremap <leader>r :w\|silent ! echo "python -m unittest discover" > send-command<cr>


"c
"autocmd FileType c set columns=90 lines=50
autocmd FileType c set cul cuc
"方便输入->
autocmd FileType c inoremap -- ->
"自动化保存并make
autocmd FileType c noremap <leader>m :make<cr>
autocmd FileType c noremap <leader>r :w\|silent ! echo "make" > send-command<cr>
autocmd FileType * noremap <leader>c :w\|silent ! echo "make clean" > send-command<cr>

autocmd FileType c set foldmethod=syntax

"cpp
autocmd FileType cpp set foldmethod=syntax
autocmd FileType cpp set cul cuc
autocmd FileType cpp inoremap -- ->
autocmd FileType cpp noremap <leader>r :w\|silent ! echo "make" > send-command<cr>


"markdown
"autocmd FileType markdown set columns=90 lines=50
"正确识别markdown文件
autocmd BufNewFile,BufRead *.md,*.markdown :set filetype=markdown
autocmd BufNewFile,BufRead *.mkdn :set filetype=tex

"autocmd BufWritePost *.mkdn make %<.pdf
"autocmd BufWritePost *.mkdn silent ! echo "make %<.pdf" > send-command
autocmd BufWritePost *.mkdn silent ! echo "make -f $pandocmakefile %<.pdf" > $fifofile
autocmd FileType markdown inoremap $ $$<left>
autocmd FileType markdown noremap <leader>r :w<cr>

"tex
"autocmd BufWritePost *.tex silent ! echo "make -f $texmakefile %<.pdf" > $fifofile
autocmd FileType tex noremap <leader>r :w\|:make %<.pdf<cr>
autocmd FileType tex inoremap $ $$<left>

"bash
au BufNewFile *.sh 0r $bashheader
"保存.vimrc时自动source
autocmd BufWritePost .vimrc nested source %

"html
"autocmd FileType html set columns=90 lines=50

"haskell
autocmd FileType haskell inoremap -- ->
autocmd FileType haskell inoremap == =>
"""""""""""""""""""""""""""""""""""""""""""""""""""""










"GUI setting
"""""""""""""""""""""""""""""""""""""""""""""""""""""
set guifont=Source\ Code\ Pro:h14
set guifontwide=PingFang\ SC
"set guifont=Hack:h13
"""""""""""""""""""""""""""""""""""""""""""""""""""""





"一些有用的函数
"""""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <leader>p :call SetPaste()<cr>

function! SetPaste()
    if &paste
        setlocal paste!
    else
        setlocal paste
    endif
endfunction

noremap <leader>s :call SetColLine()<cr>

function! SetColLine()
    set columns=90
    set lines=50
endfunction

"""""""""""""""""""""""""""""""""""""""""""""""""""""



""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Vundle vim的插件管理程序
filetype off

"必须的部分
"""""""""""""""""""""""""""""""""""""""""""""""""""""
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" 让Bundle管理Vundle
Plugin 'gmarik/Vundle.vim'
"""""""""""""""""""""""""""""""""""""""""""""""""""""

"自动配对括号引号
"Plugin 'AutoClose'
Plugin 'jiangmiao/auto-pairs'

"显示树状文件目录
Plugin 'The-NERD-tree'
"定义一下启动nerd tree的热键，我用的是Ctrl+n
noremap <C-t> :NERDTreeToggle<CR>
"自动打开nerdtree
"autocmd vimenter * NERDTree
"在只剩一个nerdtree的情况下关闭窗口
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif


"用<Tab>键自动补全
"Bundle 'ervandew/supertab'
"let g:SuperTabDefaultCompletionType = "<c-x><c-o>"

"you complete me
Plugin 'Valloric/YouCompleteMe'

"set the global extra conf
let g:ycm_global_ycm_extra_conf = '~/.vim/bundle/YouCompleteMe/.ycm_extra_conf.py'
let g:ycm_confirm_extra_conf = 0
let g:ycm_complete_in_comments=1
let g:ycm_min_num_of_chars_for_completion=1
let g:ycm_cache_omnifunc=0
let g:ycm_seed_identifiers_with_syntax=1

autocmd FileType python setlocal completeopt-=preview
autocmd FileType c setlocal completeopt-=preview
autocmd FileType cpp setlocal completeopt-=preview

autocmd BufEnter *.cpp sign define dummy
autocmd BufEnter *.cpp execute 'sign place 9999 line=1 name=dummy buffer=' . bufnr('')

"some map for goto defination
noremap <C-]> :YcmCompleter GoToDefinition<CR>
noremap <C-[> :YcmCompleter GoToDeclaration<CR>
noremap <leader>g :YcmCompleter GetType<CR>



"
"jedi vim
"Plugin 'davidhalter/jedi-vim'

"Plugin 'Shougo/neocomplete.vim'

"autocmd FileType python setlocal completeopt-=preview

""Note: This option must set it in .vimrc(_vimrc).  NOT IN .gvimrc(_gvimrc)!
"" Disable AutoComplPop.
"let g:acp_enableAtStartup = 0
"" Use neocomplete.
"let g:neocomplete#enable_at_startup = 1
"" Use smartcase.
"let g:neocomplete#enable_smart_case = 1
"" Set minimum syntax keyword length.
"let g:neocomplete#sources#syntax#min_keyword_length = 3
"let g:neocomplete#lock_buffer_name_pattern = '\*ku\*'

"" Define dictionary.
"let g:neocomplete#sources#dictionary#dictionaries = {
    "\ 'default' : '',
    "\ 'vimshell' : $HOME.'/.vimshell_hist',
    "\ 'scheme' : $HOME.'/.gosh_completions'
        "\ }

"" Define keyword.
"if !exists('g:neocomplete#keyword_patterns')
    "let g:neocomplete#keyword_patterns = {}
"endif
"let g:neocomplete#keyword_patterns['default'] = '\h\w*'

"" Plugin key-mappings.
"inoremap <expr><C-g>     neocomplete#undo_completion()
"inoremap <expr><C-l>     neocomplete#complete_common_string()

"" Recommended key-mappings.
"" <CR>: close popup and save indent.
"inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
"function! s:my_cr_function()
  "return neocomplete#close_popup() . "\<CR>"
  "" For no inserting <CR> key.
  ""return pumvisible() ? neocomplete#close_popup() : "\<CR>"
"endfunction
"" <TAB>: completion.
"inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
"" <C-h>, <BS>: close popup and delete backword char.
"inoremap <expr><C-h> neocomplete#smart_close_popup()."\<C-h>"
"inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"
"inoremap <expr><C-y>  neocomplete#close_popup()
"inoremap <expr><C-e>  neocomplete#cancel_popup()
"" Close popup by <Space>.
""inoremap <expr><Space> pumvisible() ? neocomplete#close_popup() : "\<Space>"

"" For cursor moving in insert mode(Not recommended)
""inoremap <expr><Left>  neocomplete#close_popup() . "\<Left>"
""inoremap <expr><Right> neocomplete#close_popup() . "\<Right>"
""inoremap <expr><Up>    neocomplete#close_popup() . "\<Up>"
""inoremap <expr><Down>  neocomplete#close_popup() . "\<Down>"
"" Or set this.
""let g:neocomplete#enable_cursor_hold_i = 1
"" Or set this.
""let g:neocomplete#enable_insert_char_pre = 1

"" AutoComplPop like behavior.
""let g:neocomplete#enable_auto_select = 1

"" Shell like behavior(not recommended).
""set completeopt+=longest
""let g:neocomplete#enable_auto_select = 1
""let g:neocomplete#disable_auto_complete = 1
""inoremap <expr><TAB>  pumvisible() ? "\<Down>" : "\<C-x>\<C-u>"

"" Enable omni completion.
"autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
"autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
"autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
""autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
"autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

""for python
"autocmd FileType python setlocal omnifunc=jedi#completions
"let g:jedi#completions_enabled = 0
"let g:jedi#auto_vim_configuration = 0

"" Enable heavy omni completion.
"if !exists('g:neocomplete#sources#omni#input_patterns')
  "let g:neocomplete#sources#omni#input_patterns = {}
"endif

"let g:neocomplete#force_omni_input_patterns = {}

"let g:neocomplete#force_omni_input_patterns.python =
            "\ '\%([^. \t]\.\|^\s*@\|^\s*from\s.\+import \|^\s*from \|^\s*import \)\w*'
"" alternative pattern: '\h\w*\|[^. \t]\.\w*'
""
""let g:neocomplete#sources#omni#input_patterns.php = '[^. \t]->\h\w*\|\h\w*::'
""let g:neocomplete#sources#omni#input_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
""let g:neocomplete#sources#omni#input_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'



"python-mode插件
Plugin 'klen/python-mode'
"缩进
let g:pymode_indent = 1 
"折叠
let g:pymode_folding = 0
"快速运动
let g:pymode_motion = 1
"文档
let g:pymode_doc = 1
"支持虚拟环境
let g:pymode_virtualenv = 1
"运行程序
let g:pymode_run = 0
"断点
let g:pymode_breakpoint = 0
let g:pymode_breakpoint_key = 'b'
"拼写检查
let g:pymode_lint = 1
let g:pymode_lint_on_write = 1
let g:pymode_lint_on_fly = 0
let g:pymode_lint_message = 1
let g:pymode_lint_checker = ['pyflakes','pep8']
let g:pymode_lint_ignore = "E265,E501,E127,C901,E116"
"自动补全，这里没有启用，和前面的jide-vim冲突
""map <C-l> <C-Space>
let g:pymode_rope = 1 
let g:pymode_rope_completion = 0
"let g:pymode_rope_complete_on_dot = 1
"let g:pymode_rope_completion_bind = '<Tab>'
"高亮
let g:pymode_syntax = 1
let g:pymode_syntax_all = 1
"let g:pymode_syntax_indent_errors = g:pymode_syntax_all
"let g:pymode_syntax_space_errors = g:pymode_syntax_all

"airline
"Plugin 'bling/vim-airline'
"set ttimeoutlen=50
"powerline
Plugin 'Lokaltog/vim-powerline'
set laststatus=2

"快速移动
Plugin 'Lokaltog/vim-easymotion'
"map f <Plug>(easymotion-prefix)

"另外一个快速移动工具
Plugin 'justinmk/vim-sneak'

"replace 'f' with 1-char Sneak
nmap f <Plug>Sneak_f
nmap F <Plug>Sneak_F
xmap f <Plug>Sneak_f
xmap F <Plug>Sneak_F
omap f <Plug>Sneak_f
omap F <Plug>Sneak_F
"replace 't' with 1-char Sneak
nmap t <Plug>Sneak_t
nmap T <Plug>Sneak_T
xmap t <Plug>Sneak_t
xmap T <Plug>Sneak_T
omap t <Plug>Sneak_t
omap T <Plug>Sneak_T

"autocmd ColorScheme * hi! link Sneak Normal
"autocmd ColorScheme * hi SneakLabel guifg=white guibg=magenta ctermfg=white ctermbg=magenta

"markdown高亮插件
"Plugin 'plasticboy/vim-markdown'

"高效的注释工具
Plugin 'scrooloose/nerdcommenter'

"色彩方案solarized
Plugin 'altercation/vim-colors-solarized'

Plugin 'morhetz/gruvbox'

"c support
"Bundle 'WolfgangMehner/vim-plugins/c-support'
Plugin 'c.vim'

"快速选择括号中的文本对象
Plugin 'gcmt/wildfire.vim'

map <C-s> <Plug>(wildfire-fuel)

"highlight for c++ STL
Plugin 'Mizuchi/STL-Syntax'

"Plugin 'yonchu/accelerated-smooth-scroll'

"surround
Plugin 'tpope/vim-surround'

"ctrlP
"Plugin 'kien/ctrlp.vim'

"command T
Plugin 'wincent/Command-T'

set wildignore+=*.o,.git,*.txt,*.cfg,*.pyc,*.ipynb,*.pdf,*.aux,*.eps,*.png,*.bbl,*.blg,*.out,*.toc,*.nb

"emacas-bindings
Plugin 'maxbrunsfeld/vim-emacs-bindings'

"indent guides
Plugin 'nathanaelkane/vim-indent-guides'
let g:indent_guides_enable_on_vim_startup = 1
"let g:indent_guides_auto_colors = 1
"autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=red   ctermbg=235
"autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=green ctermbg=236

"multiple cursors
Plugin 'kris89/vim-multiple-cursors'

" Disable autocomplete before multiple cursors to avoid conflict
" For neocomplete only
"function! Multiple_cursors_before()
    "exe 'NeoCompleteLock'
"endfunction

" Enable autocomplete after multiple cursors
"function! Multiple_cursors_after()
    "exe 'NeoCompleteUnlock'
"endfunction


"a.vim switch between source file and head file
Plugin 'vim-scripts/a.vim'

"html
Plugin 'mattn/emmet-vim'
let g:user_emmet_install_global = 0
autocmd FileType html,htmldjango,css EmmetInstall

"tag list
Plugin 'vim-scripts/taglist.vim'
let Tlist_Ctags_Cmd='/usr/local/bin/bin/ctags'
map <F8> :TlistToggle<cr>

"vim plugin for pandoc
"Bundle 'vim-pandoc/vim-pandoc'
"map for convinience
"nmap <leader>p !pandoc -o %:r.pdf % --latex-engine xelatex

"task list
Plugin 'vim-scripts/TaskList.vim'
nmap <leader>v <Plug>TaskList
let g:tlWindowPosition = 1

"vim git gutter
Plugin 'airblade/vim-gitgutter'

"代码片段
Plugin 'SirVer/ultisnips'
let g:UltiSnipsExpandTrigger="<leader><tab>"
let g:UltiSnipsJumpForwardTrigger="<leader><tab>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
let g:UltiSnipsEditSplit="vertical"
"Plugin 'honza/vim-snippets'


"haskell
Plugin 'dag/vim2hs'
let g:haskell_conceal_wide = 1

"Plugin 'matze/vim-tex-fold'

call vundle#end()
filetype plugin indent on     " 必须的

"折叠
autocmd FileType python set foldmethod=indent
autocmd FileType python set foldnestmax=2

"选择色彩方案
"solarized
"set background=dark
"set t_Co=256
"let g:solarized_termcolors=256
"colorscheme solarized

"gruvbox
colorscheme gruvbox
let g:gruvbox_contrast_dark='soft'

autocmd FileType tex colorscheme solarized
